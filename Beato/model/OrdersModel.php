<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina OrdersModel.php permette di gestire l'entità orders del database.
 */

    class OrdersModel {

        //Collegamento al database
        public function __construct(){
            require_once 'Database.php';
        }
        
        //Aggiunge un prodotto agli ordini dell'utente
        //Input: id dell'utente, id del prodotto
        public function addProduct($user, $product){
            $db = db_connect();
            
            $user = $db->quote($user);
            $product = $db->quote($product);
            
            $query = "INSERT INTO orders (id_user, id_product, date) VALUES ($user, $product, now());";
            $rows = $db->exec($query);
        }
        
        //Input: id dell'utente
        //Output: array contenente gli id dei prodotti acquistati e le relative date di acquisto
        public function getOrders($id){
            $array = array();
            
            $db = db_connect();
            
            $id = $db->quote($id);
            
            $query = "SELECT * FROM orders WHERE id_user=$id ORDER BY date DESC;";
            $rows = $db->query($query);
            
            $i=0;
            foreach($rows as $row){
                $array[$i]["product"] = $row["id_product"];
                $array[$i]["date"] = $row["date"];
                $i++;
            }
          
            return $array;
        }
        
        //Input: id del prodotto, id dell'utente
        //Output: true se l'utente ha comprato il prodotto, false altrimenti
        public function heBoughtIt($product, $user){
            $db = db_connect();
            
            $user = $db->quote($user);
            $product = $db->quote($product);
            
            $query = "SELECT * FROM orders WHERE id_user=$user and id_product=$product;";
            $rows = $db->query($query);
            
            return $rows->fetch();
        }
    }
?>   