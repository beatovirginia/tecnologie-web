<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina ProductModel.php permette di gestire l'entità product del database.
 */

    class ProductModel {

        //Collegamento al database
        public function __construct(){
            require_once 'Database.php';
        }
        
        //Incrementa di 1 il numero di voti di un prodotto
        //Input: id del prodotto
        public function addNRate($id){
            $db = db_connect();
            
            $nRate = $this->getNRate($id);
            $nRate++;
            
            $id = $db->quote($id);
            
            $query = "UPDATE product SET n_rate = $nRate WHERE id = $id;";
            $rows = $db->exec($query);
        }
        
        //Incrementa di 1 il numero di acquisti di un prodotto
        //Input: id del prodotto
        public function addPurchased($id){
            $previousNumber = $this->getPurchased($id);
            $previousNumber++;
            
            $db = db_connect();
            
            $id = $db->quote($id);
            $previousNumber = $db->quote($previousNumber);
            
            $query = "UPDATE product SET purchased = $previousNumber WHERE id = $id;";
            $rows = $db->exec($query);
        }
        
        //Aggiorna la valutazione di un prodotto
        //Input: nuovo voto, id del prodotto
        public function addRate($rate, $id){
            $previousRate = $this->getTrueRate($id);
            $nRate = $this->getNRate($id);
            
            $rate = (($previousRate*$nRate)+$rate)/($nRate+1);
            
            $this->addNRate($id);
            
            $db = db_connect();
            
            $id = $db->quote($id);
            
            $query = "UPDATE product SET rate = $rate WHERE id = $id;";
            $rows = $db->exec($query);
        }
        
        //Input: id del prodotto
        //Output: descrizione del prodotto
        public function getDescription($id){
            $db = db_connect();
            
            $id = $db->quote($id);
            
            $query = "SELECT description FROM product WHERE id=$id;";
            $rows = $db->query($query);
           
            $row = $rows->fetch();
          
            return $row["description"];
        }
        
        //Input: id del prodotto
        //Output: nome del prodotto
        public function getName($id){
            $db = db_connect();
            
            $id = $db->quote($id);
            
            $query = "SELECT name FROM product WHERE id=$id;";
            $rows = $db->query($query);
           
            $row = $rows->fetch();
          
            return $row["name"];
        }
        
        //Input: id del prodotto
        //Output: numero di valutazioni del prodotto
        public function getNRate($id){
            $db = db_connect();
            
            $id = $db->quote($id);
            
            $query = "SELECT n_rate FROM product WHERE id=$id;";
            $rows = $db->query($query);
           
            $row = $rows->fetch();
          
            return $row["n_rate"];
        }
        
        //Output: i 10 prodotti più scontati
        public function getOnSale() {
            $db = db_connect();
            
            $query = "SELECT * FROM product WHERE sale>0 ORDER BY sale DESC LIMIT 10;";
            $rows = $db->query($query);
            
            return $rows;
        }
        
        //Input: id del prodotto
        //Output: prezzo del prodotto senza calcolare il relativo sconto
        public function getPrice($id){
            $db = db_connect();
            
            $id = $db->quote($id);
            
            $query = "SELECT price FROM product WHERE id=$id;";
            $rows = $db->query($query);
           
            $row = $rows->fetch();
          
            return $row["price"];
        }
        
        //Input: stringa cercata dall'utente
        //Output: array contenente i prodotti che hanno la stringa nel nome
        //nota: il prezzo è già calcolato in base allo sconto
        public function getProducts($key){
            $array = array();
            
            $db = db_connect();
            
            $query = "SELECT * FROM product WHERE name LIKE '%$key%';";
            $rows = $db->query($query);
            
            $i=0;
            foreach($rows as $row){
                $array[$i]["id"]  = $row["id"];
                $array[$i]["name"] = $row["name"];
                $price = $row["price"] - ($row["price"] * ($row["sale"] / 100));
                $price = round($price, 2, PHP_ROUND_HALF_UP); //arrotondamento
                $array[$i]["price"] = $price;
                $array[$i]["rate"] = $row["rate"];
                $i++;
            }
          
            return $array;
        }
        
        //Input: id del prodotto
        //Output: numero di acquisti del prodotto
        public function getPurchased($id){
            $db = db_connect();
            
            $id = $db->quote($id);
            
            $query = "SELECT purchased FROM product WHERE id=$id;";
            $rows = $db->query($query);
           
            $row = $rows->fetch();
          
            return $row["purchased"];
        }
        
        //Input: id del prodotto
        //Output: valutazione del prodotto con arrotondamento
        public function getRate($id){
            $db = db_connect();
            
            $id = $db->quote($id);
            
            $query = "SELECT rate FROM product WHERE id=$id;";
            $rows = $db->query($query);
           
            $row = $rows->fetch();
            
            $ret = round($row["rate"], 0, PHP_ROUND_HALF_UP);
          
            return $ret;
        }
        
        //Input: id del prodotto
        //Output: sconto relativo al prodotto
        public function getSale($id){
            $db = db_connect();
            
            $id = $db->quote($id);
            
            $query = "SELECT sale FROM product WHERE id=$id;";
            $rows = $db->query($query);
           
            $row = $rows->fetch();
          
            return $row["sale"];
        }
        
        //Output: array contenente i 10 prodotti più acquistati
        public function getTrending() {
            $db = db_connect();
            
            $query = "SELECT * FROM product WHERE 1 ORDER BY purchased DESC LIMIT 10;";
            $rows = $db->query($query);
            
            return $rows;
        }
        
        //Input: id del prodotto
        //Output: valutazione del prodotto senza arrotondamento
        public function getTrueRate($id){
            $db = db_connect();
            
            $id = $db->quote($id);
            
            $query = "SELECT rate FROM product WHERE id=$id;";
            $rows = $db->query($query);
           
            $row = $rows->fetch();
          
            return $row["rate"];
        }
       
    }
?>