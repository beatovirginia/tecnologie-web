<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina CartModel.php permette di gestire l'entità cart del database.
 */

    class CartModel {

        //Collegamento al database
        public function __construct(){
            require_once 'Database.php';
        }
        
        //Aggiunge un prodotto nel carrello di un utente
        //Input: id del prodotto, id dell'utente
        public function addToCart($product, $user){
            $db = db_connect();
            
            $user = $db->quote($user);
            $product = $db->quote($product);
            
            $query = "INSERT INTO cart (id_user, id_product) VALUES ($user, $product)";
            $rows = $db->exec($query);
        }
        
        //Input: id dell'utente
        //Output: array contenente gli id dei prodotti presenti nel carrello dell'utente
        public function getCart($id){
            $array = array();
            
            $db = db_connect();
            
            $id = $db->quote($id);
            
            $query = "SELECT id_product FROM cart WHERE id_user=$id;";
            $rows = $db->query($query);
            
            $i=0;
            foreach($rows as $row){
                $array[$i] = $row["id_product"];
                $i++;
            }
          
            return $array;
        }
        
        //Input: id del prodotto, id dell'utente
        //Output: true se il prodotto è nel carrello dell'utente
        //        false altrimenti
        public function isOnCart($product, $user){
            $db = db_connect();
            
            $user = $db->quote($user);
            $product = $db->quote($product);
            
            $query = "SELECT * FROM cart WHERE id_user=$user and id_product=$product;";
            $rows = $db->query($query);
            
            return $rows->fetch();
        }
        
        //Rimuove un prodotto dal carrello di un utente
        //Input: id del prodotto, id dell'utente
        public function removeFromCart($product, $user){
            $db = db_connect();
            
            $user = $db->quote($user);
            $product = $db->quote($product);
            
            $query = "DELETE FROM cart WHERE id_user=$user and id_product=$product;";
            $rows = $db->exec($query);
        }
        
        //Rimuove tutti gli elementi presenti nel carrello di un utente
        //Input: id dell'utente
        public function resetCart($user){
            $db = db_connect();
            
            $user = $db->quote($user);
            
            $query = "DELETE FROM cart WHERE id_user = $user;";
            $rows = $db->exec($query);
        }
        
    }
?>