<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina WishlistModel.php permette di gestire l'entità wishlist del database.
 */

    class WishlistModel {

        //Collegamento al database
        public function __construct(){
            require_once 'Database.php';
        }
        
        //Aggiunge un prodotto alla wishlist di un utente
        //Input: id del prodotto, id dell'utente
        public function addToWishlist($product, $user){
            $db = db_connect();
            
            $user = $db->quote($user);
            $product = $db->quote($product);
            
            $query = "INSERT INTO wishlist (id_user, id_product) VALUES ($user, $product)";
            $rows = $db->exec($query);
        }
        
        //Input: id dell'utente
        //Output: array contenente gli id dei prodotti presenti nella wishlist degli utenti
        public function getWishlist($id){
            $array = array();
            
            $db = db_connect();
            
            $id = $db->quote($id);
            
            $query = "SELECT id_product FROM wishlist WHERE id_user=$id;";
            $rows = $db->query($query);
            
            $i=0;
            foreach($rows as $row){
                $array[$i] = $row["id_product"];
                $i++;
            }
          
            return $array;
        }
        
        //Input: id del prodotto, id dell'utente
        //Output: true se il prodotto è nella wishlist dell'utente, false altrimenti
        public function isOnWishlist($product, $user){
            $db = db_connect();
            
            $user = $db->quote($user);
            $product = $db->quote($product);
            
            $query = "SELECT * FROM wishlist WHERE id_user=$user and id_product=$product;";
            $rows = $db->query($query);
            
            return $rows->fetch();
        }
        
        //Rimuove un prodotto dalla wishlist di un utente
        //Input: id del prodotto, id dell'utente
        public function removeFromWishlist($product, $user){
            $db = db_connect();
            
            $user = $db->quote($user);
            $product = $db->quote($product);
            
            $query = "DELETE FROM wishlist WHERE id_user=$user and id_product=$product;";
            $rows = $db->exec($query);
        }
        
    }
?>