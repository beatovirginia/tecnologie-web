<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina UserModel.php permette di gestire l'entità user del database.
 */

    class UserModel {

        //Collegamento al database
        public function __construct(){
            require_once 'Database.php';
        }
        
        //Input: email di un utente
        //Output: true se l'email è presente nel database, false altirmenti
        public function existEmail($email) {
            $db = db_connect();
            
            $email = $db->quote($email);
            
            $query = "SELECT * FROM user WHERE email=$email;";
            $rows = $db->query($query);
           
            $row = $rows->fetch();
          
            return $row["id"] > 0;
        }
        
        //Input: email di un utente
        //Output: id relativo alla email
        public function getId($email){
            $db = db_connect();
            
            $email = $db->quote($email);
            
            $query = "SELECT id FROM user WHERE email=$email;";
            $rows = $db->query($query);
           
            $row = $rows->fetch();
          
            return $row["id"];
        }
        
        //Input: id dell'utente
        //Output: nome relativo all'id dell'utente
        public function getName($id){
            $db = db_connect();
            
            $id = $db->quote($id);
            
            $query = "SELECT name FROM user WHERE id=$id;";
            $rows = $db->query($query);
           
            $row = $rows->fetch();
          
            return $row["name"];
        }
        
        //Input: email di un utente, password dell'utente
        //Output: true se la coppia <email,password> è presente nel database, false altrimenti
        public function getUser($email, $password) {
            $db = db_connect();
            
            $email = $db->quote($email);
            
            $query = "SELECT * FROM user WHERE email=$email;";
            $rows = $db->query($query);
           
            $row = $rows->fetch();
          
            return $row["password"] == $password;
        }
        
        //Inserisce un nuovo record nella tabella user
        //Input: id dell'utente, nome dell'utente, email dell'utente, password dell'utente
        public function insertRow($id, $name, $email, $password) {
            $db = db_connect();
            
            $id = $db->quote($id);
            $name = $db->quote($name);
            $email = $db->quote($email);
            $password = $db->quote($password);
            
            $query = "INSERT INTO user (id, name, email, password)"
                    . "VALUES ($id, $name, $email, $password);";
            $rows = $db->exec($query);    
        }

        //Output: il più grande id presente nella tabella user
        public function maxId(){
            $db = db_connect();
            
            $query = "SELECT id FROM user ORDER BY id DESC;";
            $rows = $db->query($query);
           
            $row = $rows->fetch();
          
            return $row["id"];
        }
        
    }
?>