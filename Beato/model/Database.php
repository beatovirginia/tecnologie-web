<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina Database.php permette di aprire il database pencil
 */

//Output: l'oggetto PDO del database se riesce ad aprirlo, null altrimenti
function db_connect(){
    $dsn = 'mysql:dbname=pencil; host=localhost; port=8080';
    try {
	$db = new PDO($dsn, 'root', 'root');
        return $db;
    } catch (PDOException $ex) {
  	?>
  	<p>Sorry, a database error occurred.</p>
  	<?php
    }
    return NULL;
}

?>