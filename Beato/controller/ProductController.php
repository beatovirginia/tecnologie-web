<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina ProductController.php è il controller della vista Product.php.
 */

    require_once '../model/ProductModel.php';
    require_once '../model/OrdersModel.php';
    require_once '../model/WishlistModel.php';
    require_once '../model/CartModel.php';
    
    class ProductController {
        
        private $productModel;
        private $ordersModel;
        private $wishlistModel;
        private $cartModel;
        
        //Crea i modelli
        public function __construct(){
            $this->productModel = new ProductModel();
            $this->ordersModel = new OrdersModel();
            $this->wishlistModel = new WishlistModel();
            $this->cartModel = new CartModel();
        }
        
        //Input: id del prodotto
        //Output: descrizione del prodotto
        public function getDescription($id){
            return $this->productModel->getDescription($id);
        }
        
        //Input: id del prodotto
        //Output: nome del prodotto
        public function getName($id){
            return $this->productModel->getName($id);
        }
        
        //Input: id del prodotto
        //Output: prezzo del prodotto senza il calcolo dello sconto
        public function getPrice($id){
            return $this->productModel->getPrice($id);
        }
        
        //Input: id del prodotto
        //Output: numero di acquisti effettuati di quel prodotto
        public function getPurchased($id){
            return $this->productModel->getPurchased($id);
        }
        
        //Input: id del prodotto
        //Output: valutazione del prodotto
        public function getRate($id){
            return $this->productModel->getRate($id);
        }
        
        //Input: id del prodotto
        //Output: sconto relativo al prodotto
        public function getSale($id){
            return $this->productModel->getSale($id);
        }
        
        //Input: id del prodotto, id dell'utente
        //Output: true se l'utente ha comprato il prodotto, false altrimenti
        public function heBoughtIt($product, $user){
            return $this->ordersModel->heBoughtIt($product, $user);
        }
        
        //Input: id del prodotto, id dell'utente
        //Output: true se l'utente ha il prodotto nel carrello, false altrimenti
        public function isOnCart($product, $user){
            return $this->cartModel->isOnCart($product, $user);
        }
        
        //Input: id del prodotto, id dell'utente
        //Output: true se l'utente ha il prodotto nella wishlist, false altrimenti
        public function isOnWishlist($product, $user){
            return $this->wishlistModel->isOnWishlist($product, $user);
        }
     
    }
?>