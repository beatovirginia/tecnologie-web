<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina Login.php è il controller della vista Login.php.
 */

    require_once '../model/UserModel.php';
    
    class LoginController {
        
        private $userModel;
        
        //Crea il modello
        public function __construct(){
            $this->userModel = new UserModel();
        }
        
        //Input: email dell'utente
        //Output: id associato alla mail
        public function getId($email){
            return $this->userModel->getId($email);
        }
        
        //Input: email dell'utente, password dell'utente
        //Output: true se la coppia <email,password> è presente nel database
        //        false altrimenti
        public function verifyData($email, $password){
            return $this->userModel->getUser($email, $password);
        }
         
    }
?>

