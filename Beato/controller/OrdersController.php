<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina OrdersController.php è il controller della vista Orders.php.
 */

    require '../model/OrdersModel.php';
    require '../model/ProductModel.php';
    
    class OrdersController {
        
        private $ordersModel;
        private $productModel;
        
        //Crea i modelli
        public function __construct(){
            $this->ordersModel = new OrdersModel();
            $this->productModel = new ProductModel();
        }
        
        //Input: id del prodotto
        //Output: nome del prodotto
        public function getName($id){
            return $this->productModel->getName($id);
        }
        
        //Input: id dell'utente
        //Output: lista degli ordini effettuati dall'utente
        public function getOrders($id){
            return $this->ordersModel->getOrders($id);
        }
        
        //Input: id del prodotto
        //Output: prezzo del prodotto senza l'applicazione dello sconto
        public function getPrice($id){
            return $this->productModel->getPrice($id);
        }
        
        //Input: id del prodotto
        //Output: sconto relativo a quel prodotto
        public function getSale($id){
            return $this->productModel->getSale($id);
        }
    }
?>