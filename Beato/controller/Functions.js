$(document).ready(function(){ //quando la pagina è caricata
    
    var idUser = $("#idUser").val();
    
    //Rende tutti i div con class = product draggable
    $(".product").draggable({ 
        containment: 'document',
        revert: 'invalid',
        helper: 'clone',
        zIndex: 100
    });
      
    //Rende tutti i div con id = cart droppable
    $("#cart").droppable({
        drop:function(e, ui){
            $.ajax({
                url: "../controller/jsFunctions/addToCart.php", 
                type: "post",
                data:{
                    product: $(ui.draggable).attr('data-id'),
                    user: idUser
                }
            }).done(function() {
                changeColor("#cart");
                $("head").append('<link href="../view/css/Cart.css" type="text/css" rel="stylesheet" >');
                $("section").load("../view/Cart.php", showError);
            });
        }
    });
    
    //Rende tutti i div con id = wishlist droppable
    $("#wishlist").droppable({
        drop:function(e, ui){
            $.ajax({
                url: "../controller/jsFunctions/addToWishlist.php", 
                type: "post",
                data:{
                    product: $(ui.draggable).attr('data-id'),
                    user: idUser
                }
            }).done(function() {
                changeColor("#wishlist");
                $("head").append('<link href="../view/css/Wishlist.css" type="text/css" rel="stylesheet" >');
                $("section").load("../view/Wishlist.php", showError);
            });
        }
    });
    
    //Quando il div con id = orders viene cliccato, esso cambia colore
    //di sfondo e nella section appare la classe Orders.php
    $("#orders").click(function(){
        changeColor("#orders");
        $("head").append('<link href="../view/css/Orders.css" type="text/css" rel="stylesheet" >');
        $("section").load("../view/Orders.php", showError);
    });

    //Quando il div con id = wishlist viene cliccato, esso cambia colore
    //di sfondo e nella section appare la classe Wishlist.php
    $("#wishlist").click(function(){
        changeColor("#wishlist");
        $("head").append('<link href="../view/css/Wishlist.css" type="text/css" rel="stylesheet" >');
        $("section").load("../view/Wishlist.php", showError);
    });

    //Quando il div con id = cart viene cliccato, esso cambia colore
    //di sfondo e nella section appare la classe Cart.php
    $("#cart").click(function(){
        changeColor("#cart");
        $("head").append('<link href="../view/css/Cart.css" type="text/css" rel="stylesheet" >');
        $("section").load("../view/Cart.php", showError);
    });
    
    //Quando viene premuto il bottone della ricerca nella section appare
    //la pagina Search.php. Il parametro è la stringa cercata.
    //Il relativo foglio di stile è Wishlist.css
    $("#search").click(function(){
        var value = $("#idProdSearch").val();
        $("head").append('<link href="../view/css/Wishlist.css" type="text/css" rel="stylesheet" >');
        $("#cart").css("background-color", "#D8DBD7");
        $("#wishlist").css("background-color", "#D8DBD7");
        $("#orders").css("background-color", "#D8DBD7");
        $("section").load("../view/Search.php", {id:value}, showError);
    });
    
    //Quando un prodotto viene cliccato nella section appare la pagina Product.php del
    //prodotto stesso; il parametro è l'id del prodotto.
    $("section").on("click", ".product", function() {
        var value = $(this).attr('data-id');
        $("head").append('<link href="../view/css/Product.css" type="text/css" rel="stylesheet" >');
        $("section").load("../view/Product.php", {id:value}, showError);
    });
    
    //Quando la pagina di un prodotto viene caricata, il suo bottone "remove
    //from wishlist" contiene il listener che permette di aprire lo script per
    //rimuovere il prodotto dalla wishlist.
    $("section").on("click", "#removeProdWishlist", function() {
        $.ajax({
            url: "../controller/jsFunctions/removeFromWishlist.php", 
            type: "post",
            data:{
                product: $("#idProduct").val(),
                user: idUser
            }
        }).done(function() {
            changeColor("#wishlist");
            $("head").append('<link href="../view/css/Wishlist.css" type="text/css" rel="stylesheet" >');
            $("section").load("../view/Wishlist.php", showError);
        });
    });
    
    //Quando la pagina di un prodotto viene caricata, il suo bottone "add
    //to wishlist" contiene il listener che permette di aprire lo script per
    //aggiungere il prodotto alla wishlist.
    $("section").on("click", "#addProdWishlist", function() {
        $.ajax({
            url: "../controller/jsFunctions/addToWishlist.php", 
            type: "post",
            data:{
                product: $("#idProduct").val(),
                user: idUser
            }
        }).done(function() {
            changeColor("#wishlist");
            $("head").append('<link href="../view/css/Wishlist.css" type="text/css" rel="stylesheet" >');
            $("section").load("../view/Wishlist.php", showError);
        });
    });
    
    //Quando la pagina di un prodotto viene caricata, il suo bottone "remove
    //from cart" contiene il listener che permette di aprire lo script per
    //rimuovere il prodotto dal carrello.
    $("section").on("click", "#removeProdCart", function() {
        $.ajax({
            url: "../controller/jsFunctions/removeFromCart.php", 
            type: "post",
            data:{
                product: $("#idProduct").val(),
                user: idUser
            }
        }).done(function() {
            changeColor("#cart");
            $("head").append('<link href="../view/css/Cart.css" type="text/css" rel="stylesheet" >');
            $("section").load("../view/Cart.php", showError);
        });
    });
    
    //Quando la pagina di un prodotto viene caricata, il suo bottone "add
    //to cart" contiene il listener che permette di aprire lo script per
    //aggiungere il prodotto all carrello.
    $("section").on("click", "#addProdCart", function() {
        $.ajax({
            url: "../controller/jsFunctions/addToCart.php", 
            type: "post",
            data:{
                product: $("#idProduct").val(),
                user: idUser
            }
        }).done(function() {
            changeColor("#cart");
            $("head").append('<link href="../view/css/Cart.css" type="text/css" rel="stylesheet" >');
            $("section").load("../view/Cart.php", showError);
        });
    });
    
    //Quando la pagina di un prodotto viene caricata, il suo bottone "add
    //rate" contiene il listener che permette di aprire lo script per
    //aggiungere una valutazione.
    $("section").on("click", "#addRate", function() {
        var idProd = $("#idProduct").val();  
        var rate = $("#valRate").val();
        console.log(rate);
        if(rate>0 && rate<6){
            $.ajax({
                url: "../controller/jsFunctions/addRate.php", 
                type: "post",
                data:{
                    product: idProd,
                    rate: rate
                }
            }).done(function() {
                $("section").load("../view/Product.php", {id:idProd}, showError);
            });
        }else{
            $("#productPage .invisibleError").css("visibility", "visible");
        }
    });
    
    //Quando viene caricata la pagina Cart.php, il suo bottone "buy" contiene
    //il listener che permette di aprire lo script per completare l'ordine.
    $("section").on("click", "#buy", function() {  
        var address = $("#address").val();
        if(address!==""){
            $.ajax({
                url: "../controller/jsFunctions/buy.php", 
                type: "post",
                data:{
                    user: idUser
                }
            }).done(function(){
                $("section").load("../view/Cart.php", showError);
            });
        }else{
            $("#cartPage").append("<div class='error'> Invalid address </div>");
        }
    });
});

//funzione che avvisa che la pagina da aprire non è stata caricata
function showError(responseTxt, statusTxt, xhr){
    if(statusTxt == "error"){
        $("section").append("<div class='error'> Page not found, please retry </div>");
    }
}

//Input: l'elemento che si vuole schiarire (a scelta tra wishlist, cart o orders)
function changeColor(a){
    switch(a){
        case "#wishlist":
            $("#wishlist").css("background-color", "#F0F2EF");
            $("#orders").css("background-color", "#D8DBD7");
            $("#cart").css("background-color", "#D8DBD7");
            break;
        case "#orders":
            $("#orders").css("background-color", "#F0F2EF");
            $("#wishlist").css("background-color", "#D8DBD7");
            $("#cart").css("background-color", "#D8DBD7");
            break;
        case "#cart":
            $("#cart").css("background-color", "#F0F2EF");
            $("#wishlist").css("background-color", "#D8DBD7");
            $("#orders").css("background-color", "#D8DBD7");
            break;
    }      
}