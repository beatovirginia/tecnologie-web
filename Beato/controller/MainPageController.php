<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina MainPageController.php è il controller della vista MainPage.php.
 */

    require_once '../model/UserModel.php';
    
    class MainPageController {
        
        private $userModel;
        
        //Crea il modello
        public function __construct(){
            $this->userModel = new UserModel();
        }
        
        //Input: id dell'utente
        //Output: nome dell'utente
        public function getName($id){
            return $this->userModel->getName($id);
        }
    }
?>