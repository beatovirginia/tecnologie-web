<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina Logout.php è utile per terminare la sessione.
 * Rimanda alla pagina di login.
 */

    if (!isset($_SESSION)) { session_start(); }

    session_unset();
    session_destroy();
    
    include_once("../view/Login.php");
?>