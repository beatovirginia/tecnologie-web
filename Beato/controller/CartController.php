<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina CartController.php è il controller della vista Cart.php.
 */

    require '../model/CartModel.php';
    require '../model/ProductModel.php';
    
    class CartController {
        
        private $cartModel;
        private $productModel;
        
        //Creazione dei modelli
        public function __construct(){
            $this->cartModel = new CartModel();
            $this->productModel = new ProductModel();
        }
        
        //Input: id dell'utente
        //Output: il prezzo totale dei prodotti presenti nel carrello dell'utente
        public function getAmount($id){
            $cart = $this->cartModel->getCart($id);
            $amount = 0;
            foreach($cart as $product){
                $price = $this->productModel->getPrice($product);
                $price = $price - ($price * ($this->productModel->getSale($product) / 100));
                $price = round($price, 2, PHP_ROUND_HALF_UP);
                $amount = $amount + $price;
            }
            return $amount;
        }
        
        //Input: id dell'utente
        //Output: tutti i prodotti appartenenti al carrello di un utente
        public function getCart($id){
            return $this->cartModel->getCart($id);
        }
        
        //Input: id del prodotto
        //Output: nome del prodotto
        public function getName($id){
            return $this->productModel->getName($id);
        }
        
        //Input: id del prodotto
        //Output: prezzo del prodotto senza aver calcolato lo sconto
        public function getPrice($id){
            return $this->productModel->getPrice($id);
        }
        
        //Input: id del prodotto
        //Output: sconto relativo al prodotto
        public function getSale($id){
            return $this->productModel->getSale($id);
        }
    }
?>