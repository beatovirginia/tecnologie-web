<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina WishlistController.php è il controller della vista Wishlist.php.
 */

    require '../model/WishlistModel.php';
    require '../model/ProductModel.php';
    
    class WishlistController {
        
        private $wishlistModel;
        private $productModel;
        
        //Crea i modelli
        public function __construct(){
            $this->wishlistModel = new WishlistModel();
            $this->productModel = new ProductModel();
        }
        
        //Input: id del prodotto
        //Output: nome del prodotto
        public function getName($id){
            return $this->productModel->getName($id);
        }
        
        //Input: id del prodotto
        //Output: prezzo del prodotto senza l'applicazione dello sconto
        public function getPrice($id){
            return $this->productModel->getPrice($id);
        }
        
        //Input: id del prodotto
        //Output: valutazione del prodotto
        public function getRate($id){
            return $this->productModel->getRate($id);
        }
        
        //Input: id del prodotto
        //Output: sconto relativo al prodotto
        public function getSale($id){
            return $this->productModel->getSale($id);
        }
        
        //Input: id dell'utente
        //Output: wishlist dell'utente
        public function getWishlist($id){
            return $this->wishlistModel->getWishlist($id);
        }
    }
?>