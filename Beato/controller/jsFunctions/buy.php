<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina buy.php permette di acquistare i prodotti presenti nel carrello.
 * Si occupa di aggiungere ogni prodotto agli ordini e di aumentare il
 * relativo numero di acquisti di quel prodotto. Inoltre, svuota il carrello.
 * E' utile ad una funzione presente in Functions.js.
 */

    require_once '../../model/CartModel.php';
    require_once '../../model/OrdersModel.php';
    require_once '../../model/ProductModel.php';
    
    $user = $_POST["user"];
    $cartModel = new CartModel();
    $ordersModel = new OrdersModel();
    $productModel = new ProductModel();
    
    $cart = $cartModel->getCart($user);
    foreach($cart as $product){
        $ordersModel->addProduct($user, $product);
        $productModel->addPurchased($product);
    }
    $cartModel->resetCart($user);
?>