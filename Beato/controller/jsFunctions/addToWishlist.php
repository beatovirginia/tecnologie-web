<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina addToWishlist.php permette di aggiungere un prodotto alla wishlist.
 * Se il prodotto è già nella wishlist non fa niente.
 * E' utile ad una funzione presente in Functions.js.
 */

    require_once '../../model/WishlistModel.php';
    
    $model = new WishlistModel();
    
    if(!$model->isOnWishlist($_POST["product"], $_POST["user"])){
        $model->addToWishlist($_POST["product"], $_POST["user"]);
    }
?>