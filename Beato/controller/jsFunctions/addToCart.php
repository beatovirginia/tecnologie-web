<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina addToCart.php permette di aggiungere un prodotto al carrello.
 * Se il prodotto è già nel carrello non fa niente.
 * E' utile ad una funzione presente in Functions.js.
 */

    require_once '../../model/CartModel.php';
    
    $model = new CartModel();
    
    if(!$model->isOnCart($_POST["product"], $_POST["user"])){
        $model->addToCart($_POST["product"], $_POST["user"]);
    }
?>