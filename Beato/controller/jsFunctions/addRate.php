<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina addRate.php permette di aggiungere un voto ad un prodotto.
 * E' utile ad una funzione presente in Functions.js.
 */

    require_once '../../model/ProductModel.php';
    
    $model = new ProductModel();
    
    $model->addRate($_POST["rate"], $_POST["product"]);
?>