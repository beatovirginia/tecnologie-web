<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina removeFromWishlist.php permette di rimuovere un prodotto dalla wishlist.
 * E' utile ad una funzione presente in Functions.js.
 */

    require_once '../../model/WishlistModel.php';
    
    $model = new WishlistModel();
    
    if($model->isOnWishlist($_POST["product"], $_POST["user"])){
        $model->removeFromWishlist($_POST["product"], $_POST["user"]);
    }
?>