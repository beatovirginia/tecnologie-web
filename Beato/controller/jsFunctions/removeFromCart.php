<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina removeFromCart.php permette di rimuovere un prodotto dal carrello.
 * E' utile ad una funzione presente in Functions.js.
 */

    require_once '../../model/CartModel.php';
    
    $model = new CartModel();
    
    if($model->isOnCart($_POST["product"], $_POST["user"])){
        $model->removeFromCart($_POST["product"], $_POST["user"]);
    }
?>