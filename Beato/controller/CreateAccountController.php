<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina CreateAccountController.php è il controller della vista CreateAccount.php.
 */

    require_once '..\model\UserModel.php';
    
    class CreateAccountController {
        
        private $userModel;
        
        //Creazione del modello
        public function __construct(){
            $this->userModel = new UserModel();
        }
        
        //Input: email dell'utente
        //Output: id associato alla mail
        public function getId($email){            
            return $this->userModel->getId($email);
        }
        
        //Input: nome dell'utente, email dell'utente, password dell'utente
        //Output: true se il nuovo utente è stato registrato,
        //        false se la email è già presente nel database 
        public function insertData($name, $email, $password){
            $maxId = $this->userModel->maxId();
            if($this->userModel->existEmail($email)){
                return false;
            }else{                
                $this->userModel->insertRow($maxId+1, $name, $email, $password);
                return true;
            } 
        }
    }
?>