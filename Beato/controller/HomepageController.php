<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina HomepageController.php è il controller della vista Homepage.php.
 */

    require_once '../model/ProductModel.php';
    
    class HomepageController {
        
        private $productModel;
        
        //Crea il modello
        public function __construct(){
            $this->productModel = new ProductModel();
        }
        
        //Output: array dei prodotti scontati
        public function getOnSale(){
            return $this->productModel->getOnSale();
        }
        
        //Output: array dei prodotti più acquistati
        public function getTrending(){
            return $this->productModel->getTrending();
        }
        
    } 
?>