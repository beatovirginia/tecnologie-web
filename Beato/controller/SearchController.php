<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina SearchController.php è il controller della vista Search.php.
 */

    require '../model/ProductModel.php';
    
    class SearchController {
        
        private $productModel;
        
        //Crea il modello
        public function __construct(){
            $this->productModel = new ProductModel();
        }
        
        //Input: stringa cercata dall'utente
        //Output: tutti i prodotti che hanno la stringa nel nome
        public function getProducts($key){
            return $this->productModel->getProducts($key);
        }
    }
?>