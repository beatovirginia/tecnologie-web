-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8080
-- Generation Time: Jan 20, 2019 at 04:04 PM
-- Server version: 5.6.34-log
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pencil`
--

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id_user` int(10) NOT NULL,
  `id_product` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cart`
--

INSERT INTO `cart` (`id_user`, `id_product`) VALUES
(4, 1),
(4, 2),
(4, 3),
(4, 4),
(4, 7),
(4, 9),
(4, 13),
(6, 1),
(6, 2),
(6, 3),
(6, 9),
(6, 10),
(7, 1),
(7, 2),
(7, 3),
(9, 1),
(9, 2),
(9, 3),
(10, 1),
(10, 3),
(10, 4),
(10, 5),
(11, 3),
(13, 0),
(13, 1),
(14, 1),
(15, 1),
(15, 2),
(15, 3),
(17, 3),
(17, 8),
(20, 2),
(20, 4),
(21, 4),
(21, 5),
(22, 7),
(23, 10);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id_user` int(10) NOT NULL,
  `id_product` int(10) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id_user`, `id_product`, `date`) VALUES
(1, 1, '2019-01-08'),
(1, 1, '2019-01-20'),
(1, 2, '2019-01-08'),
(1, 2, '2019-01-20'),
(1, 3, '2019-01-08'),
(1, 3, '2019-01-19'),
(1, 4, '2019-01-08'),
(1, 5, '2019-01-08'),
(1, 5, '2019-01-19'),
(1, 6, '2019-01-08'),
(1, 7, '2019-01-08'),
(1, 8, '2019-01-08'),
(1, 9, '2019-01-08'),
(1, 15, '2019-01-20'),
(1, 16, '2019-01-20'),
(1, 20, '2019-01-20'),
(2, 1, '2019-01-11'),
(2, 2, '2019-01-11'),
(2, 3, '2019-01-11'),
(2, 4, '2019-01-11'),
(2, 5, '2019-01-11'),
(2, 6, '2019-01-11'),
(2, 7, '2019-01-11'),
(3, 1, '2019-01-15'),
(3, 2, '2019-01-15'),
(3, 3, '2019-01-15'),
(3, 5, '2019-01-15'),
(5, 1, '2019-01-11'),
(5, 1, '2019-01-20'),
(5, 3, '2019-01-11'),
(5, 3, '2019-01-20'),
(5, 4, '2019-01-11'),
(5, 4, '2019-01-20'),
(5, 5, '2019-01-20'),
(5, 7, '2019-01-20'),
(5, 9, '2019-01-20'),
(5, 10, '2019-01-11'),
(5, 13, '2019-01-11'),
(5, 13, '2019-01-20'),
(5, 17, '2019-01-20'),
(8, 1, '2019-01-08'),
(8, 2, '2019-01-08'),
(8, 2, '2019-01-11'),
(8, 3, '2019-01-08'),
(8, 3, '2019-01-11'),
(8, 4, '2019-01-08'),
(8, 5, '2019-01-11'),
(8, 7, '2019-01-11'),
(8, 11, '2019-01-11'),
(8, 12, '2019-01-11'),
(8, 13, '2019-01-08'),
(10, 1, '2019-01-11'),
(10, 2, '2019-01-11'),
(10, 3, '2019-01-11'),
(11, 3, '2019-01-15'),
(11, 10, '2019-01-15'),
(11, 13, '2019-01-15'),
(17, 1, '2019-01-19'),
(17, 3, '2019-01-19'),
(17, 4, '2019-01-19'),
(17, 10, '2019-01-19'),
(18, 1, '2019-01-19'),
(18, 2, '2019-01-19'),
(18, 3, '2019-01-19'),
(18, 5, '2019-01-19'),
(18, 8, '2019-01-19'),
(18, 10, '2019-01-19'),
(20, 3, '2019-01-17'),
(21, 2, '2019-01-19');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` decimal(5,2) NOT NULL,
  `description` text NOT NULL,
  `rate` decimal(5,2) NOT NULL,
  `n_rate` int(11) NOT NULL DEFAULT '1',
  `sale` int(2) NOT NULL,
  `purchased` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `name`, `price`, `description`, `rate`, `n_rate`, `sale`, `purchased`) VALUES
(1, 'Parker 1953170 Jotter Ballpoint Pen, Stainless Steel with Chrome Trim, Medium Point Blue Ink', 11.83, '- Streamlined style with clean lines for a fresh take on the iconic PARKER Jotter design\r\n- Stainless steel barrel accented by high-shine trims and an arrowhead clip\r\n- Signature retractable design opens and closes with a satisfying click\r\n- Fitted with Quick flow ballpoint refill for optimal ink flow and a smooth writing experience; blue ink; medium tip; also accepts PARKER gel refills\r\n- Packaged in a distinctive PARKER gift box', 2.20, 40, 39, 16),
(2, 'Moleskine Classic Hard Cover - Hard Cover Notebook for Writing, Sketching, Journals', 11.89, '- CLASSIC MOLESKINE NOTEBOOK: Moleskine classic notebooks are perfect notebooks for writing journals, a daily diary, or note taking in college classes or meetings. Moleskine notebooks are beloved by travelers & bullet journalists for their slim design.\r\n- DURABLE COVER & ELASTIC CLOSURE: Hold writing projects & notes in your Moleskine notebook with an elastic closure band & inner storage folders. Leather-like classic Moleskine cover & thick, ivory paper pages are perfect for writing with fountain pens.\r\n- GIFT QUALITY NOTEBOOKS: Moleskine planners, journals and notebooks come in hardcover or softcover and colors like black, red, blue, green and brown. The binding and cover have a durable finish, designed for daily journaling, writing and sketching.\r\n- DELUXE QUALITY PAGES: Moleskine\'s thick, ivory paper pages in a hardcover Moleskine notebook, softcover Moleskine notebook, cahier or volant journal, or Moleskine planner are perfectly textured for writing with a ballpoint pen, fountain pen, or pencil.\r\n- MOLESKINE IS LOVED FOR A REASON: Writers, students, artists, professionals & travelers love Moleskine notebooks & planners. From lined to unlined, dotted to graph paper there\'s a Moleskine notebook for your needs. Try one & you\'ll understand the passion.', 4.00, 1, 0, 9),
(3, 'Sakura Pigma 30062 Micron Blister Card Ink Pen Set, Black, Ass\'t Point Sizes 6CT Set', 10.83, '- Pigma micron ink pen set\r\n- Experience smooth, skip-free writing and crisp ink colors that leave consistent lettering and lines every time\r\n- Nib sizes for precise line width are 0.20mm, 0.25mm, 0.30mm, 0.35mm, 0.45mm and 0.50mm respectively\r\n- Achieved universal appeal and is carried by technical professionals, artists and crafters\r\n- Available in 6-piece per pack and black color\r\n- Fine point pen ideal for drawing and writing\r\n- Archival quality ink for use in acid-free environments', 3.60, 5, 30, 17),
(4, 'Tombow 56187 Dual Brush Pen Art Markers, Pastel, 10-Pack. Blendable, Brush and Fine Tip Markers', 15.90, '- Ideal for fine art, brush lettering, faux calligraphy, illustrations, water color illustrations, journaling and more!\r\n- Pastel color 10 pcs assortment set in plastic case\r\n- Great for Fine art, illustrations, doodling, journaling, Hand lettering and more\r\n- Odor-less & non-bleed water-based ink\r\n- Ideal for Professional Fine art & craft', 3.33, 3, 50, 7),
(5, 'Stabilo BOSS Original Highlighter, Pastels - 6-color Set', 9.80, '- CLASSIC DESIGN IN STYLISH PASTEL COLOR - STABILO BOSS ORIGINAL Pastel is our highlighter range with a difference. The soft, fashionable colors will give your work a subtle but stylish look.\r\n- ANTI-DRY-OUT TECHNOLOGY - Don\'t worry if you accidentally forget to put the cap back on: thanks to STABILO Anti-Dry-Out technology, the STABILO BOSS Original can be left with its cap off for up to 4 hours without drying out.\r\n- STRONG BUIL QUALITY - The tip will last for a very long time. The two-line widths, 2mm + 5mm - ideal for highlighting texts of various sizes as well as for drawing lines of different thicknesses.\r\n- WATER-BASED INK - Its ink is water-based and therefore completely harmless to health. These features make the\r\n- STABILO BOSS ORIGINAL a perfect highlighter that you can rely on.\r\n- 6-COLOR SET - The set includes the 6 subtle, trendy pastel colors: creamy peach, hint of mint, pink blush, lilac haze, milky yellow, and touch of turquoise', 3.00, 1, 10, 8),
(6, 'Whitelines Notebook, Background Disappears When You Scan Pages With Whitelines Free App', 15.90, '- 11″ x 8.5″ Graph paper notebook , 70 Sheets\r\n- The 20# paper is tinted in a light grey tone which makes it less tiring for the eye than using a white paper\r\n- The background disappears when you scan the page with Whitelines free app\r\n- The Whitelines App works in conjunction with Whitelines paper and is the easiest way to scan, save and share your notes.\r\n- The white lines support your writing and drawing without the distractions from conventional dark lines\r\n- Instantly create an electronic file of whatever is on the page, while eliminating the background', 1.00, 1, 0, 2),
(7, 'Pentel EnerGel Deluxe RTX Retractable Liquid Gel Pen, 0.5mm, Needle Tip, Black Ink, 3 Pen per Pack ', 5.91, '- Smooth blend of liquid and gel ink\r\n- Quick drying ink\r\n- 0.5mm needle tip\r\n- Refillable with Pentel LRN5 refills\r\n- Pack includes 3 pens', 4.50, 2, 15, 6),
(8, 'LIHIT LAB Pen Case, 7.9 x 2 x 4.7 inches, Black', 8.30, '- Large, wide open pockets allowing you to store a variety of stationery items.\r\n- Sophisticated inner structure with straps and pouches to store stationery\r\n- 5 storage pockets on front cover.\r\n- Formal design suitable for both office & school use\r\n- Choose from 4 colors(Orange, Yellow Green, Brown, Black).\r\n- Exterior Material:Polyester', 2.80, 5, 0, 3),
(9, 'Sakura Arch Evolutional Foam Erasers, 5-Pack, White ', 5.18, 'Sakura Arch Evolutional Foam Erasers, 5-Pack, White (Japan Imported)', 3.00, 1, 0, 2),
(10, 'rOtring 600 0.5mm Black Barrel Mechanical Pencil', 22.83, '- An iconic tool meant for a lifetime of use. The full-metal body provides ideal weight balance for fatigue-free writing and drawing\r\n- Brass mechanism allows precision lead advancement. Lead hardness grade indicator for rapid identification when working\r\n- Hexagonal shape avoids sliding on drawing tables. Design, pattern and size of metallic grip zone enable working for long hours without slipping\r\n- The fixed lead guidance sleeve prevents breakage and gives you a clear page view for precise ruler-based drawing\r\n- Limited warranty: guaranteed for 2 years from original purchase date against defects in materials or workmanship', 3.67, 6, 15, 6),
(11, 'Strength & Deep & Smooth -Uni-ball Extra Fine Diamond Infused Pencil Leads', 7.80, '- Uni develops the lead of the top quality\r\n- The goods handled by our shop are high quality and popular in Japan,and we sell these items with reasonable set pricing.\r\n- Intensity of the lead was sharply strengthened by blending the particles of a diamond.\r\n- We have other reasonable sets,and please try to directly request your hopes.\r\n- Since particles have aligned by high density, it can write distinctly still more deeply', 5.00, 1, 0, 1),
(12, 'Rhodia Classic Orange Notepad 8.3x11.7 Lined', 10.70, '- 80 sheets in a Pad\r\n- Perforated on top\r\n- Stiff back cover\r\n- Acid-free paper\r\n- Made in France', 4.00, 1, 0, 1),
(13, 'iDream365 Hard Protective EVA Carrying Case/Pouch/Holder', 7.98, '- 2017 New black eva carrying case,storage space for stylus touch pen,and some small accessories.\r\n- Semi-hard case made of high quality EVA material,Material: EVA & Nylon; Weight:70g;\r\n- Interior pocket for your stylus pen and some accessories;\r\n- External dimension:6.7\'\' (L) x 3.5\" (W) x1.0\" (H); inside dimension: 6.1\" (L) x 3.0\" (W) x0.8\"(H)\r\n- Stylus pens and phones are for illustration and not included.', 2.00, 1, 10, 4),
(14, 'Moleskine Limited Edition Petit Prince Hard Cover 2019 12 Month Daily Planner', 25.61, '- MOLESKINE DAILY PLANNER: Moleskine daily planners are perfect for business planning, travel planning , bullet journals and fitness journals. Each day gets its own page, so you can track complicated projects and stay on top of your busy schedule.\r\n- INCREASE & TRACK PRODUCTIVITY: Moleskine daily planners come with calendar grids & pages for to do lists, bullet journaling or other scheduling & project tracking styles. Moleskine daily planners make it easy to reach your goals.\r\n- GIFT QUALITY PLANNERS: Moleskine planners, journals & notebooks come in hardcover or softcover & colors like black, red, blue, green & brown. The binding & cover have a durable finish, designed for daily journaling, writing & sketching.\r\n- DELUXE QUALITY PAGES: Moleskine\'s thick, ivory paper pages in a hardcover Moleskine notebook, softcover Moleskine notebook, cahier or volant journal, or Moleskine planner are perfectly textured for writing with a ballpoint pen, fountain pen, or pencil.\r\n- MOLESKINE IS LOVED FOR A REASON: Writers, students, artists, professionals & travelers love Moleskine notebooks & planners. From lined to unlined, dotted to graph paper there\'s a Moleskine notebook for your needs. Try one & you\'ll understand the passion.', 5.00, 1, 90, 0),
(15, 'Pilot Metropolitan Collection Fountain Pen', 14.95, '- Pilot\'s sleek Metropolitan Collection signature pens\r\n- Finest quality at a mid-range price\r\n- Fine nib\r\n- Ships with 1 Pilot black ink cartridge and 1 Pilot Press Plate Converter\r\n- Black barrel', 4.50, 1, 85, 1),
(16, 'Waterman 1.7 oz Ink Bottle for Fountain Pens, Intense Black', 7.41, '- Bottled ink for all WATERMAN Fountain Pens\r\n- Liquid ink produces an intense line in brilliant colors\r\n- The choice of generations, black collides dramatically with the page to underscore the impact of your words with enduring clarity\r\n- The ritual of filling ink from a bottle enhances the noble experience of using classic fountain pens\r\n- Composition:Dye based', 3.50, 1, 70, 1),
(17, 'Waterman 1.7 oz Ink Bottle for Fountain Pens, Audacious Red', 10.40, '- Bottled ink for all WATERMAN Fountain Pens\r\n- Liquid ink produces an intense line in brilliant colors\r\n- Boldly surging from the color spectrum, - Audacious Red whispers power and passion with primal sensuality\r\n- The ritual of filling ink from a bottle enhances the noble experience of using classic fountain pens\r\n- Skilfully made in France, every WATERMAN pen echoes the genius of founder Lewis Edson - WATERMAN, inventor of the first reliable fountain pen in 1883', 3.25, 2, 10, 1),
(18, 'AT-A-GLANCE 2019 Standard Diary Daily Reminder, 5-3/4\" x 8-1/4\", Medium, Red', 48.99, '- YEAR-ROUND PLANNING. Medium diary covers 12 months from January 2019-December 2019. Keep track of important deadlines, special events and more with clear organization and professional style.\r\n- INK BLEED RESISTANCE. Plan your schedule without fear of distracting ink bleeding. Our improved, high-quality paper is designed for superior ink bleed resistance, which keeps plans neat and legible.\r\n- DAILY VIEWS. Diary is dated, with one day per page. Features ruled pages with 29 lines for notetaking. Pages measure 5-3/4\" x 8-1/4\".\r\n- PROFESSIONAL DESIGN. Take charge of your schedule with modern style. Diary has a Red Moiré cover, a durable hardback and bookbound binding to help keep pages secure for prolonged use.\r\n- ENHANCED ORGANIZATION. Expand your organizational skills with specialized pages. Includes: Contacts. Monthly expenses.', 0.50, 1, 80, 0),
(19, 'AT-A-GLANCE 2019 Desk Calendar, Desk Pad, 21-3/4\" x 17\", Standard, Ruled Blocks', 10.00, '- YEAR-ROUND PLANNING. Desk calendar covers 12 months, January 2019-December 2019. Julian dates included. Keep track of deadlines, special events and more with clear organization and professional style.\r\n- INK BLEED RESISTANCE. Plan your schedule without fear of distracting ink bleeding. Our improved, high-quality paper is designed for superior ink bleed resistance, which keeps plans neat and legible.\r\n- MONTHLY VIEWS. Great for long-term planning, each month features ruled daily blocks for neat scheduling, a full year of reference calendars and a notetaking space. Pages measure 21-3/4\" x 17\".\r\n- VERSATILE PLACEMENT. Place your schedule where you can easily view it. Desk pad has a black headband with eyelets for optional wall hanging, which improves visibility. Comes with 24 desk pads.', 3.00, 1, 75, 0),
(20, 'Post-it 654 Notes, 3 in x 3 in, Canary Yellow, 12 Pads/Pack, 100 Sheets/Pad', 9.99, '- 3 in x 3 in, Canary Yellow\r\nPost-it Notes stick securely and remove cleanly\r\n- Unique adhesive designed for use on paper or in planners\r\n- America\'s #1 favorite sticky note\r\n- Leave reminders or call out important information\r\n- 12 Pads/Pack, 100 Sheets/Pad\r\n- Post-it Notes are recyclable', 5.00, 1, 95, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`) VALUES
(1, 'Virginia', 'beatovirginia@mail.com', 'Virginia'),
(2, 'Mario', 'rossimario@mail.com', 'MarioMario'),
(3, 'Giuseppe', 'verdigiuseppe@mail.com', 'Giuseppe'),
(4, 'Maria', 'franzosomaria@mail.com', 'MariaMaria'),
(5, 'Riccardo', 'emanuelriccardo@mail.com', 'Riccardo'),
(6, 'Claudio', 'spallaclaudio@mail.com', 'Claudio'),
(7, 'Mara', 'vairettomara@mail.com', 'MaraMara'),
(8, 'Elio', 'beatoelio@mail.com', 'ElioElio'),
(9, 'Bart', 'bartsimpson@mail.com', 'BartBart'),
(10, 'Roberta', 'rollandroberta@mail.com', 'Roberta'),
(11, 'Cloe', 'sarteurcloe@mail.com', 'CloeCloe'),
(12, 'Simone', 'sarteursimone@mail.com', 'Simone'),
(13, 'Fabio', 'masalafabio@mail.com', 'FabioFabio'),
(14, 'Lilia', 'destefanislilia@mail.com', 'LiliaLilia'),
(15, 'Armando', 'beatoarmando@mail.com', 'Armando'),
(16, 'Secondino', 'vairettosecondino@mail.com', 'Secondino'),
(17, 'Lara', 'croftlara@mail.com', 'LaraLara'),
(18, 'Simona', 'husimona@mail.com', 'Simona'),
(19, 'Lisa', 'chenlisa@mail.com', 'LisaLisa'),
(20, 'Luca', 'damatoluca@mail.com', 'LucaLuca'),
(21, 'Bud', 'spencerdub@mail.com', 'BudBud'),
(22, 'Terence', 'hillterence@mail.com', 'Terence'),
(23, 'Ilaria', 'masalailaria@mail.com', 'Ilaria'),
(24, 'Lisa', 'simpsonlisa@mail.com', 'LisaLisa');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `id_user` int(10) NOT NULL,
  `id_product` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wishlist`
--

INSERT INTO `wishlist` (`id_user`, `id_product`) VALUES
(0, 0),
(1, 1),
(1, 5),
(1, 6),
(1, 10),
(1, 13),
(1, 15),
(1, 18),
(1, 19),
(4, 2),
(4, 4),
(4, 5),
(4, 10),
(5, 3),
(5, 4),
(5, 5),
(5, 10),
(5, 13),
(8, 1),
(8, 2),
(8, 3),
(8, 4),
(10, 1),
(10, 5),
(11, 1),
(11, 3),
(11, 4),
(14, 4),
(15, 3),
(17, 1),
(17, 4),
(17, 5),
(17, 10),
(18, 5),
(20, 1),
(21, 1),
(21, 2),
(21, 3),
(21, 4),
(21, 5),
(22, 1),
(23, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id_user`,`id_product`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id_user`,`id_product`,`date`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`,`email`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id_user`,`id_product`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
