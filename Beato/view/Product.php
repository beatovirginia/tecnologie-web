<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina Product.php mostra le informazioni riguardanti un prodotto, in
 * particolare viene presentata una immagine, il titolo, la descrizione, la
 * valutazione e il prezzo.
 * Se un utente ha già acquistato il prodotto ha la possibilità di valutarlo
 * grazie agli appositi textfield e bottone.
 * Se un utente ha il prodotto nella wishlist ha la possibilità di toglierlo
 * grazie all'apposito bottone.
 * Se un utente ha il prodotto nel carrello ha la possibilità di toglierlo grazie
 * all'apposito bottone.
 */

    if (!isset($_SESSION)){ 
        session_start(); 
    }
    if (!isset($_SESSION["id"])){    
        header("Location: ../view/Login.php");
    }

    require '../controller/ProductController.php';
    $productController = new ProductController();
    
    $id = $_POST["id"];
    $name = $productController->getName($id);
    $price = $productController->getPrice($id);
    $description = $productController->getDescription($id);
    $rate = $productController->getRate($id);
    $sale = $productController->getSale($id);
    $purchased = $productController->getPurchased($id);
?>

<input type="hidden" id="idProduct" name="idProduct" value="<?=$id?>">

<div id="productPage">
    <div>
        <img src="img/products/<?=$id?>.jpg" alt="product's image">
    </div>
    
    <div id="name">
        <span><?= $name ?></span>
    </div>
    
    <?php
        if($sale==0){
            ?>
            <div id="price">
                <span>Price:</span> <?= $price ?>€
            </div>
            <?php
        }else{
            $euro = $price - ($price * ($sale / 100));
            $euro = round($euro, 2, PHP_ROUND_HALF_UP);
            ?>
            <div id="price">
                <span>Price:</span> 
                <del><?= $price ?></del>
                <ins><?= $euro ?></ins>
                € (<?= $sale ?>% discount)
            </div>
            <?php
        }
    ?>
    
    <div id="rate">
        <div>
            <span>Rate: </span>
        </div>
        <?php
            for($i=0 ; $i<$rate ; $i++){
                ?>
                <img src="img/star.png" alt="star">
                <?php
            }
        ?>
    </div>
    
    <div id="description">
        <span>Description: </span>
        <pre><?= $description ?></pre>
    </div>
            
    <?php
        if($productController->heBoughtIt($id, $_SESSION["id"])){
            ?>
            <div class='invisibleError'> Invalid rate </div>
            <div>
                Add rate (from 1 to 5): <input type="number" id="valRate" name="valRate">
                <button type="button" id="addRate">Add rate</button>
            </div>
            <br>
            <?php
        }
        
        if($productController->isOnWishlist($id, $_SESSION["id"])){
            ?>
            <button type="button" id="removeProdWishlist">Remove from wishlist</button>
            <?php
        }else{
            ?>
            <button type="button" id="addProdWishlist">Add to wishlist</button>
            <?php 
        }
        
        if($productController->isOnCart($id, $_SESSION["id"])){
            ?>
            <button type="button" id="removeProdCart">Remove from cart</button>
            <?php
        }else{
            ?>
            <button type="button" id="addProdCart">Add to cart</button>
            <?php 
        }
    ?>      
</div>