<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina Homepage.php è quella che viene aperta appena l'utente fa il
 * login. E' divisa in due sezioni: la parte superiore elenca i 5 prodotti con
 * uno sconto più alto, mentre la parte inferiore elenca i 5 prodotti più
 * acquistati.
 * Premendo su un prodotto qualunque viene aperta la rispettiva pagina di
 * presentazione del prodotto stesso.
 */
    
    if (!isset($_SESSION)){ 
        session_start(); 
    }
    if (!isset($_SESSION["id"])){                
        header("Location: ../view/Login.php");
    }

    require '../controller/HomepageController.php';
    $homepageController = new HomepageController();
?>

<div id="homepage">
    <div class="title">On sale:</div>
    <div class="inlineProducts">
        <?php
            $array = $homepageController->getOnSale();
            printElements($array);
        ?>
    </div>

    <hr>
    
    <div class="title">Trending:</div>
    <div class="inlineProducts">
        <?php
            $array = $homepageController->getTrending();
            printElements($array);
        ?>
    </div>
</div>

<?php
function printElements($array){
    for($i = 0 ; $i < 5 ; $i++){
        $product = $array->fetch();
        ?>
        <div class="product" data-id="<?=$product["id"]?>">
            <img src="img/products/<?=$product["id"]?>.jpg" alt="product's image">
            <?php
                $euro = $product["price"] - ($product["price"] * ($product["sale"] / 100));
                $euro = round($euro, 2, PHP_ROUND_HALF_UP); //arrotondamento
            ?>
            <p><?=$euro?> €</p>
        </div>
        <?php
    }
}
?>