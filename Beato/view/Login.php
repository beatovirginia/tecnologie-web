<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina Login.php permette ad un utente già registrato di effettuare il
 * login attraverso l'inserimento della propria email e password nei rispettivi
 * textfield. Questa pagina è dotata di 2 bottoni: uno per convalidare il 
 * login ed entrare nella homepage, l'altro per indirizzare l'utente alla
 * pagina della registrazione nel caso fosse nuovo.
 */

    $flag = false; 
    //Primo utilizzo di flag:
    //  se true bisogna andare in mainPage,
    //  altrimenti restare in login.
    //Secondo utilizzo: per vedere se è la prima volta che si arriva in login.php o sono sbagliati i dati
    
    if (isset($_REQUEST["email"])) {
        require '../controller/LoginController.php';
        $controller = new LoginController();
        $flag = $controller->verifyData($_REQUEST["email"], $_REQUEST["password"]);
    }
    
    if($flag){
        $id = $controller->getId($_REQUEST["email"]);
        session_start();
        $_SESSION["id"] = $id;
        require 'MainPage.php';
    }else{
        if (isset($_REQUEST["email"])) {
            //l'utente ha sbagliato ad inserire la propria email o password
            $flag = true;
        }else{
            //è la prima volta che viene caricata la pagina
            $flag = false;
        }
?>

        <!DOCTYPE html>

        <html lang="en">
            <head>
                <title>Pencil</title>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta name="description" content="User Login" >
                <link href="/view/css/Style.css" type="text/css" rel="stylesheet" >
                <link href="/view/img/favicon.png" type="image/gif" rel="shortcut icon">
            </head>
            <body>

                <div>
                    <img src="/view/img/logoTxt.png" alt="pencil's logo">
                </div>
                <?php
                    if($flag){
                        ?>
                        <div class="error"> wrong email or password </div>
                        <?php
                    }
                ?>
                <form action="../view/Login.php" method="post">
                    <fieldset>
                        <legend>Sign In</legend>
                        <div>
                            <div>
                                Email
                                <br>
                                <input type="email" name="email" size="20" required>
                            </div>
                            <div>
                                Password
                                <br>
                                <input type="password" name="password" size="20" pattern=".{6,}" title="Six or more characters" required>
                            </div>
                        </div>
                        <input type="submit" value="Sign in">
                    </fieldset>
                </form>
                <form action="../view/CreateAccount.php" target="_self" method="post" enctype="text/plain">
                    <div>
                        New to Pencil?
                        <br>
                        <input type="submit" value="Create Your Account Pencil">
                    </div>
                </form>
                        
            </body>
        </html>
        
        <?php 
    } 
?>

