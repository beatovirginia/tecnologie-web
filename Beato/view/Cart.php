<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina Cart.php permette di visualizzare l'elenco dei prodotti presenti
 * nel carrello di un utente.
 * La pagina è divisa in due sezioni: la sezione di sinistra contiene l'elenco
 * dei prodotti presenti nel carrello, mentre la sezione di destra contiene
 * il prezzo totale degli acquisti, un textfield per inserire l'indirizzo di
 * spedizione e il bottone per procedere all'acquisto.
 * Se il carrello è vuoto a sinistra compare esclusivamente il messaggio 
 * "Your cart is empty".
 * Premendo su un prodotto qualunque viene aperta la rispettiva pagina di
 * presentazione del prodotto stesso.
 */

    if (!isset($_SESSION)){ 
        session_start();
    }
    if (!isset($_SESSION["id"])){                
        header("Location: ../view/Login.php");
    }

    require '../controller/CartController.php';
    $cartController = new CartController();
    
    $cart = $cartController->getCart($_SESSION["id"]);
?>

<div id="cartPage">
    <?php
        if(count($cart)==0){
            ?>
            <h1> Your cart is empty </h1>
            <?php
        }else{
            ?>
            <section>
            <?php
                for($i=0 ; $i<count($cart) ; $i++){
                    ?>
                    <div class="product" data-id="<?=$cart[$i]?>">
                        <img src="img/products/<?=$cart[$i]?>.jpg" alt="product's image">
                        <div>
                            <p class="title"><?=$cartController->getName($cart[$i])?></p>
                            <?php
                                $price = $cartController->getPrice($cart[$i]);
                                $euro = $price - ($price * ($cartController->getSale($cart[$i]) / 100));
                                $euro = round($euro, 2, PHP_ROUND_HALF_UP);
                            ?>
                            <p>Price: <?=$euro?>€</p>
                        </div>
                    </div>
                    <hr>
                    <?php
                }
            ?>
            </section>

            <aside>
                <p class="title">Amount: <?=$cartController->getAmount($_SESSION["id"])?>€</p>
                <div>
                    Address: 
                    <input name="address" id="address" type="text">
                    <br>
                </div>
                <button type="button" id="buy">Buy</button>
            </aside>
            <?php
        }
    ?>
</div>