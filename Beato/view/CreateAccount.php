<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina CreateAccount.php permette la registrazione di un utente nuovo.
 * Sono presenti tre campi in cui inserire rispettivamente il nome, la email
 * e la password ed un bottone per procedere all'autenticazione.
 */

    $flag = false;
    $passwordNotMatch = false;
    
    if (isset($_REQUEST["password"]) && ($_REQUEST["password"]!=$_REQUEST["verifyPassword"])) {
        $_REQUEST["password"] = null;
        $_REQUEST["name"] = null;
        $flag = false;
        $passwordNotMatch = true;
    }
    
    if (isset($_REQUEST["name"])) {
        require_once '../controller/CreateAccountController.php';
        $createAccountController = new CreateAccountController();
        $flag = $createAccountController->insertData($_REQUEST["name"], $_REQUEST["email"], $_REQUEST["password"]);
    }
    
    if($flag){
        $id = $createAccountController->getId($_REQUEST["email"]);
        session_start();
        $_SESSION["id"] = $id;
        require '../view/MainPage.php';
    }else{
        if (isset($_REQUEST["name"])) {
            //l'utente ha inserito una email esistente
            $flag = true;
        }else{
            //è la prima volta che viene caricata la pagina
            $flag = false;
        }
        ?>

        <!DOCTYPE html>

        <html lang="en">
            <head>
                <title>Pencil</title>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta name="description" content="User Login" >
                <link href="/view/css/Style.css" type="text/css" rel="stylesheet" >
                <link href="/view/img/favicon.png" type="image/gif" rel="shortcut icon">
            </head>
            <body>
                <div>
                    <a href="MainPage.php"> 
                        <img src="/view/img/logoTxt.png" alt="Pencil's logo" />
                    </a>
                </div>
                <?php
                    if($flag){
                        ?>
                        <div class="error"> email already exist </div>
                        <?php
                    }
                    if($passwordNotMatch){
                        ?>
                        <div class="error"> password must match </div>
                        <?php
                    }
                ?>
                <form action="../view/CreateAccount.php" method="post">
                    <fieldset>
                        <legend>Sign In</legend>
                        <div>
                            <div>
                                Name
                                <br>
                                <input type="text" name="name" size="20" required>
                            </div>
                            <div>
                                Email
                                <br>
                                <input type="email" name="email" size="20" pattern="[a-z.]+@[a-z]+.[a-z]{2,}$" title="Enter a valid email address" required>
                            </div>
                            <div>
                                Password
                                <br>
                                <input type="password" name="password" size="20" pattern=".{6,}" title="Six or more characters" required>
                            </div>
                            <div>
                                Re-enter password
                                <br>
                                <input type="password" name="verifyPassword" size="20" pattern=".{6,}" title="Six or more characters" required>
                            </div>
                        </div>
                        <input type="submit" value="Create your Pencil account">
                    </fieldset>
                </form>
            </body>
        </html>

        <?php
    }
?>