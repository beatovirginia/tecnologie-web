<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina Orders.php permette di vedere l'elenco dei prodotti acquistati
 * dall'utente che ha effettuato il login.
 * Se non sono ancora stati fatti acquisti compare la scritta "You have 
 * not placed any orders". Se l'ordine non è ancora arrivato la data di arrivo
 * è evidenziata dal colore di testo rosso.
 * Premendo su un prodotto qualunque viene aperta la rispettiva pagina di
 * presentazione del prodotto stesso.
 */
    
    if (!isset($_SESSION)){ 
        session_start(); 
    }
    if (!isset($_SESSION["id"])){                
        header("Location: ../view/Login.php");
    }
    
    require '../controller/OrdersController.php';
    $ordersController = new OrdersController();
    
    $orders = $ordersController->getOrders($_SESSION["id"]);
?>

<div id="ordersPage">
    <?php
        if(count($orders)>0){
            foreach($orders as $product){
                ?>
                <div class="product" data-id="<?=$product["product"]?>">
                    <?php
                        $today = strtotime("now");
                        $date = strtotime($product["date"] . ' + 10 days');
                        if($date > $today){
                            $date = date("jS F, Y", $date);
                            ?>
                            <p id="dateR">Delivery date: <?=$date?></p>
                            <?php
                        }else{
                            $date = date("jS F, Y", $date);
                            ?>
                            <p id="date">Delivery date: <?=$date?></p>
                            <?php 
                        } 
                    ?>
                    <img src="img/products/<?=$product["product"]?>.jpg" alt="product's image">
                    <div>
                        <p><?=$ordersController->getName($product["product"])?></p>
                        <?php
                            $price = $ordersController->getPrice($product["product"]);
                            $euro = $price - ($price * ($ordersController->getSale($product["product"]) / 100));
                            $euro = round($euro, 2, PHP_ROUND_HALF_UP); //arrotondamento
                        ?>
                        <p>Price: <?=$euro?>€</p>
                    </div>
                </div>
                <hr>
                <?php
            }
        }else{
            ?>
            <h1>You have not placed any orders</h1>
            <?php
        }
    ?>
</div>