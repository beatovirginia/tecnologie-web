<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina Wishlist.php permette di vedere l'elenco dei prodotti inseriti
 * nella lista dei desideri da parte dell'utente che ha effettuato il login.
 * Se la wishlist è vuota compare esclusivamente il messaggio "Your wishlist is empty".
 * Premendo su un prodotto qualunque viene aperta la rispettiva pagina di
 * presentazione del prodotto stesso.
 */
    
    if (!isset($_SESSION)){ 
        session_start(); 
    }
    if (!isset($_SESSION["id"])){                
        header("Location: ../view/Login.php");
    }

    require '../controller/WishlistController.php';
    $wishlistController = new WishlistController();
    
    $wishlist = $wishlistController->getWishlist($_SESSION["id"]);
?>

<div id="wishlistPage">
    <?php
        if(count($wishlist)>0){
            for($i=0 ; $i<count($wishlist) ; $i++){
                ?>
                <div class="product" data-id="<?=$wishlist[$i]?>">
                    <img src="img/products/<?=$wishlist[$i]?>.jpg" alt="product's image">
                    <div>
                        <p id="title"><?=$wishlistController->getName($wishlist[$i])?></p>
                        <?php
                            $price = $wishlistController->getPrice($wishlist[$i]);
                            $euro = $price - ($price * ($wishlistController->getSale($wishlist[$i]) / 100));
                            $euro = round($euro, 2, PHP_ROUND_HALF_UP); //arrotondamento
                        ?>
                        <p>Price: <?=$euro?>€</p>
                        <div id="rate">
                            <div>Rate: </div>
                            <?php
                                $rate = $wishlistController->getRate($wishlist[$i]);
                                for($y=0 ; $y<$rate ; $y++){
                                    ?>
                                    <img src="img/star.png" alt="star">
                                    <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <hr>
                <?php
            }
        }else{
            ?>
            <h1>Your wishlist is empty</h1>
            <?php
        }
    ?>
</div>




