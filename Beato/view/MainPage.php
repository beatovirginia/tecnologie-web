<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina MainPage.php è quella contenente l'header, il footer e la barra
 * laterale dalla quale si può accedere agli ordini, alla wishlist e al carrello.
 * La "section" inizialmente contiene Homepage.php, ma è il punto in cui
 * vengono caricate dinamicamente tutte le pagine.
 */
    if (!isset($_SESSION)){ 
        session_start(); 
    }
    if (!isset($_SESSION["id"])) {                
        header("Location: ../view/Login.php");
    }

    require_once '../controller/MainPageController.php';
    $mainPageController = new MainPageController();
?>

<!DOCTYPE html>

<html lang="en">
    <head>
        <title>Pencil</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="User Login" >
        <link href="/view/css/Style.css" type="text/css" rel="stylesheet" >
        <link href="/view/css/Homepage.css" type="text/css" rel="stylesheet" >
        <link href="/view/img/favicon.png" type="image/gif" rel="shortcut icon">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
        <script src="../controller/Functions.js"></script>
        
    </head>
    <body>
        <input type="hidden" name="idUser" id="idUser" value="<?=$_SESSION["id"]?>">
        <header>
            <div>
                <a href="MainPage.php"> 
                    <img src="/view/img/logoTxt.png" alt="Pencil's logo" />
                </a>
            </div>
            <div id="searchDiv">
                <input id="idProdSearch" type="text" placeholder="Type here" required>
                <button type="button" id="search">Search</button>
            </div>
            <div>
                <?php
                    $name = $mainPageController->getName($_SESSION["id"]);
                ?>
                <p>Welcome <?=$name?>!<p>
                <form action="../controller/Logout.php" method="get">
                    <input id="submitLogout" type="submit" value="Logout">
                </form>
            </div>
        </header>
        <div id="container">
            <aside>
                <div id="orders">
                    <img src="/view/img/delivery.png" alt="orders">
                    <p> orders <p>
                </div>
                <div id="wishlist">
                    <img src="/view/img/wishlist.png" alt="wishlist">
                    <p> wishlist <p>
                </div>
                <div id="cart">
                    <img src="/view/img/cart.png" alt="cart">
                    <p> cart <p>
                </div>
            </aside>
            <section>
                <?php 
                    require 'Homepage.php';
                ?>
            </section>
        </div>
        <footer>
            <div>Author: Beato Virginia</div>
            <div>
                <a href="Privacy.html">Privacy policy</a>
            </div>
            <div>
                <a href="http://validator.w3.org/check/referer"> 
                    <img src="http://webster.cs.washington.edu/w3c-html.png" alt="Validate html" />
                </a>
                <a href="http://jigsaw.w3.org/css-validator/check/referer">
                    <img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Validate CSS" />
                </a>
            </div>
        </footer>
    </body>
</html>
