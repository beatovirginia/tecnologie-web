<?php
/* Beato Virginia
 * Pencil: un sito di ecommerce per prodotti di cancelleria
 * 
 * La pagina Search.php è quella che viene aperta in seguito ad una ricerca
 * effettuata nell'apposita barra di ricerca presente nell'header di ogni pagina.
 * Contiene un elenco di tutti i prodotti che contengono nel nome la stringa
 * cercata dall'utente.
 * Premendo su un prodotto qualunque viene aperta la rispettiva pagina di
 * presentazione del prodotto stesso.
 */

    if (!isset($_SESSION)){ 
        session_start();
    }
    if (!isset($_SESSION["id"])){                
        header("Location: ../view/Login.php");
    }
    
    require '../controller/SearchController.php';
    $searchController = new SearchController();
    
    $products = $searchController->getProducts($_POST["id"]);
?>

<div id="wishlistPage"> <!-- il css è lo stesso -->
    <?php
        if(count($products)>0){
            foreach($products as $product){
                ?>
                <div class="product" data-id="<?=$product["id"]?>">
                    <img src="img/products/<?=$product["id"]?>.jpg" alt="product's image">
                    <div>
                        <p id="title"><?=$product["name"]?></p>
                        <p>Price: <?=$product["price"]?>€</p>
                        <div id="rate">
                            <div>Rate: </div>
                            <?php
                                for($i=0 ; $i<$product["rate"] ; $i++){
                                    ?>
                                    <img src="img/star.png" alt="star">
                                    <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <hr>
                <?php
            }
        }else{
            ?>
            <h1>The search did not return any results</h1>
            <?php
        }
    ?>
</div>