<!-- Beato Virginia, Lab 04 (NerdLuv) -->
<!-- Signup page, for a new account -->

<?php include("top.html"); ?>

<form action="signup-submit.php" target="_self" method="post" enctype="multipart/from-data">
    <fieldset>
        <legend>New User Signup:</legend>
        <p>
            <strong>Name:</strong>
            <input type="text" name="name" maxlength="16">
        </p>
        <p>
            <strong>Gender:</strong>
            <label>
                <input type="radio" name="gender" value="M"> 
                Male 
            </label>
            <label>
                <input type="radio" name="gender" value="F" checked="checked"> 
                Female 
            </label>
        </p>
        <p>
            <strong>Age:</strong>
            <input type="text" name="age" size="6" maxlength="2">
        </p>
        <p>
            <strong>Personality type:</strong>
            <input type="text" name="personality" size="6" maxlength="4">
            (<a href="http://www.humanmetrics.com/cgi-win/JTypes2.asp">Don't know your type?</a>)
        </p>
        <p>
            <strong>Favorite OS:</strong>
            <select name="favoriteOS">
                <option>Windows</option>
                <option>Mac OS X</option>
                <option selected="selected">Linux</option>
            </select>
        </p>
        <p>
            <strong>Seeking age:</strong>
            <input type="text" name="agefrom" size="6" maxlength="2" placeholder="min"> 
            to
            <input type="text" name="ageto" size="6" maxlength="2" placeholder="max">
        </p>
        <p>
            <input type="submit" value="Sign Up" >
        </p>
    </fieldset>
</form>

<?php include("bottom.html"); ?>
