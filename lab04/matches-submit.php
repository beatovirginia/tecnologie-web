<!-- Beato Virginia, Lab 04 (NerdLuv) -->
<!-- Matches list -->

<?php 
    include("top.html"); 
    $name = $_REQUEST["name"];
?>

<div><strong>Mathes for <?=$name?></strong></div>

<?php
    $lines = file("singles.txt", FILE_IGNORE_NEW_LINES);
    // search the name insered by the user
    foreach($lines as $line){
        $explodedLine = explode(",", $line);
        if($explodedLine[0] == $name){
            $partnerLine = $explodedLine;
        }
    }
    // compare two users and print if they are compatibles
    foreach($lines as $line){
        $explodedLine = explode(",", $line);
        if(areCompatibles($partnerLine, $explodedLine)){
            ?>
            <div class="match">
                <img src="http://www.cs.washington.edu/education/courses/cse190m/12sp/homework/4/user.jpg" alt="user image" >
                <p><?=$explodedLine[0]?></p>
                <ul>
                    <li><strong>gender:</strong><?=$explodedLine[1]?></li>
                    <li><strong>age:</strong><?=$explodedLine[2]?></li>
                    <li><strong>type:</strong><?=$explodedLine[3]?></li>
                    <li><strong>OS:</strong><?=$explodedLine[4]?></li>
                </ul>
            </div>
            <?php
        }
    }
?>

<?php 

include("bottom.html");

// return true if the users are compatibles
function areCompatibles($partnerLine, $line){    
    if($partnerLine[1] == $line[1]){
        return false;
    }
    if($partnerLine[5] > $line[2]){
        return false;
    }
    if($partnerLine[6] < $line[2]){
        return false;
    }
    if($partnerLine[4] != $line[4]){
        return false;
    }
    for($i=0 ; $i<=3 ; $i++){
        if($partnerLine[3][$i] == $line[3][$i]){
            return true;
        }
    }
    return false;
}
                        
?>