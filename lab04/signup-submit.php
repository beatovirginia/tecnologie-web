<!-- Beato Virginia, Lab 04 (NerdLuv) -->
<!-- Register data from signup.php -->

<?php 
    include("top.html"); 
    $name = $_REQUEST["name"];
    $gender = $_REQUEST["gender"];
    $age = $_REQUEST["age"];
    $personality = $_REQUEST["personality"];
    $favoriteOS = $_REQUEST["favoriteOS"];
    $ageFrom = $_REQUEST["agefrom"];
    $ageTo = $_REQUEST["ageto"];
    
    $arrayText = array($name, $gender, $age, $personality, $favoriteOS, $ageFrom, $ageTo);
    $text = implode(",", $arrayText);
    file_put_contents("singles.txt", $text, FILE_APPEND);
    file_put_contents("singles.txt", "\n", FILE_APPEND);
?>

<p>
    <strong>Thank you!</strong>
</p>

<p>
    Welcome to NerdLuv, <?=$name?>!
</p>

<p>
    Now <a href="matches.php">log in to see your matches!</a>
</p>

<?php include("bottom.html"); ?>
