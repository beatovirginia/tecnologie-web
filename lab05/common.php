<!--
    Beato Virginia, lab05
    Pagina contenente la funzione per aprire il database e quella per ovviare
    al problema di attori omonimi.
-->

<?php
// Apre il database
function db_connect(){
    $dsn = 'mysql:dbname=imdb_small; host=localhost; port=8080';
    try {
	$db = new PDO($dsn, 'root', 'root');
        return $db;
    } catch (PDOException $ex) {
  	?>
  	<p>Sorry, a database error occurred.</p>
  	<?php
    }
    return NULL;
}

// Risolve il problema di attori omonimi. Restituisce -1 se l'attore non è
// presente nel database e il suo nome viceversa
function solvedAmbiguity($firstname, $lastname){
    $db = db_connect();
    
    $firstname_like = $db->quote($firstname . " %");
    $firstname = $db->quote($firstname);
    $lastname = $db->quote($lastname);
    
    $query = "SELECT first_name, film_count FROM actors WHERE (first_name LIKE $firstname_like OR first_name = $firstname) AND last_name = $lastname ORDER BY id DESC";
    $rows = $db->query($query);
    
    $name = -1;
    $number = -1;
    
    foreach($rows as $row){
        
        if($row['film_count'] >= $number){
            $number = $row['film_count'];
            $name = $row['first_name'];
        }
    }
    
    return $name;
}

?>