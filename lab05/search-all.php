<!--
    Beato Virginia, lab05
    Pagina contenente una tabella che elenca tutti i film in cui ha partecipato
    un determinato attore. Se l'attore non è presente nel database non verrà
    stampata la tabella, ma una frase che segnala il problema.
-->

<?php 
    include("top.html");
    include_once("common.php"); 
    $firstname = $_GET["firstname"];
    $lastname = $_GET["lastname"];
    
    $db = db_connect();
?>

<h1>Results for <?=$firstname?> <?=$lastname?></h1>

<?php
$name = solvedAmbiguity($firstname, $lastname);

if($name != -1){ // L'attore è presente nel database
    
    $firstname = $db->quote($name);
    $lastname = $db->quote($lastname);
    $query = "SELECT movies.name, movies.year
              FROM movies JOIN roles ON movies.id = roles.movie_id
                    JOIN actors ON roles.actor_id = actors.id
              WHERE actors.first_name = $firstname AND actors.last_name = $lastname
              ORDER BY movies.year DESC, movies.name;";
    $rows = $db->query($query);

    ?>
    
    <table>
        <caption>All films</caption>
        <tr>
            <th>#</th>
            <th>Title</th>
            <th>Year</th>
        </tr>
        <?php
        $i = 1;
        foreach ($rows as $row) {
            ?>
            <tr>
                <td><?=$i?></td>
                <td><?=$row["name"]?></td>
                <td><?=$row["year"]?></td>
            </tr>
            <?php
            $i++;
        }
        ?>
    </table>

<?php
}else{ // L'attore non è presente nel database
    ?>
    <p>Actor <?=$firstname?> <?=$lastname?> not found <p>
    <?php
}
?>
<?php include("bottom.html"); ?>
