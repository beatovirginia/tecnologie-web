<!--
    Beato Virginia, lab05
    Pagina contenente una tabella dove sono presenti i film in cui hanno collaborato
    Kevin Bacon e l'attore inserito dall'utente. Se non è presente l'attore
    inserito oppure se non ha fatto film assieme a Bacon apparirà una frase
    che segnala il problema.
-->

<?php 
    include("top.html");
    include_once("common.php"); 
    $firstname = $_GET["firstname"];
    $lastname = $_GET["lastname"];
    
    $db = db_connect();
?>

<h1>Results for <?=$firstname?> <?=$lastname?></h1>

<?php
$name = solvedAmbiguity($firstname, $lastname);

if($name != -1){ // L'attore è presente nel database
    
    $firstname = $db->quote($name);
    $lastname = $db->quote($lastname);
    $query =    "SELECT m.name, m.year 
                FROM actors AS a1 join roles AS r1 ON a1.id=r1.actor_id 
                    join movies AS m ON r1.movie_id = m.id 
                    join roles AS r2 on m.id = r2.movie_id 
                    join actors AS a2 ON r2.actor_id = a2.id 
                WHERE a2.first_name = 'Kevin' AND a2.last_name = 'Bacon' 
                    AND a1.first_name = $firstname AND a1.last_name = $lastname;
                ORDER BY m.year DESC, m.name;";
    $rows = $db->query($query);
    
    if($rows->rowCount() > 0){ // L'attore ha lavorato con Kevin Bacon almeno una volta
        
        // Eliminazione degli apici inseriti da quote
        $firstname = trim($firstname, "'");
        $lastname = trim($lastname, "'");
        
        ?>
        <table>
            <caption>Film with <?=$firstname?> <?=$lastname?> and Kevin Bacon</caption>
            <tr>
                <th>#</th>
                <th>Title</th>
                <th>Year</th>
            </tr>
            <?php
            $i = 1;
            foreach ($rows as $row) {
                ?>
                <tr>
                    <td><?=$i?></td>
                    <td><?=$row["name"]?></td>
                    <td><?=$row["year"]?></td>
                </tr>
                <?php
                $i++;
            }
            ?>
        </table>

    <?php 
    }else{ // L'attore non ha mai lavorato con Kevin Bacon
        // Eliminazione degli apici inseriti da quote
        $firstname = trim($firstname, "'");
        $lastname = trim($lastname, "'");
        ?>

        <p><?=$firstname?> <?=$lastname?> wasn't in any films with Kevin Bacon</p>
        
        <?php
    }

}else{ // Attore non presente nel database
    ?>
    <p>Actor <?=$firstname?> <?=$lastname?> not found <p>
    <?php
}

include("bottom.html"); 
?>