Some 10 years after expected, The Simpsons Movie couldn't give a doodle in a doughnut hole about expectations anyway. It may deliver what we've already got, but it leaves no doubt why we got it in the first place.
FRESH
Geoff Pevere
Toronto Star