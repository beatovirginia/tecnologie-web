<!--
    Beato Virginia
    Terzo anno, sezione di Reti e sistemi informatici
    Terzo esercizio da consegnare
-->

<!DOCTYPE html>
<html lang="en">
    <head> 
        <title>Rancid Tomatoes</title>
        <link href="http://courses.cs.washington.edu/courses/cse190m/11sp/homework/2/rotten.gif" type="image/gif" rel="icon">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link href="movie.css" type="text/css" rel="stylesheet">
    </head>
    
    <body>  
        
        <?php
            $movie=$_GET["film"]; // Movie's name
            $info = file("$movie/info.txt", FILE_IGNORE_NEW_LINES); // Movie's infos
            $overview = file("$movie/overview.txt", FILE_IGNORE_NEW_LINES); // Movie's overview
            $review_files = glob("$movie/review*.txt");
            $reviews = array(); // Movie's reviews
            foreach ($review_files as $filename) {
                array_push($reviews, file("$filename"));
            }
        ?>
        
        <div id = "banner">
            <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/banner.png" alt="Rancid Tomatoes">
        </div>        
              
        <!-- Movie's name -->
        <h1> <?php print"$info[0] ($info[1])";?> </h1>
                
        <div id = "main">
            
            <!-- Movie's overview -->
            <div id = "right">
                
                <!-- Overview's image -->
                <div>
                    <img src="<?=$movie;?>/overview.png" alt="general overview">
                </div>
                
                <!-- Overview's informations -->  
                <dl>
                    <?php
                        foreach($overview as $o){
                            $s1 = explode(":", $o);
                            if(strpos($s1[0], 'SYNOPSIS') !== false || strpos($s1[0], 'RATING') !== false){
                                ?>
                                <dt><?=$s1[0];?></dt>
                                <dd><?=$s1[1];?></dd>
                                <?php
                            }else{
                                $s2 = explode(", ", $s1[1]);
                                ?>
                                <dt><?=$s1[0];?></dt>
                                <?php
                                foreach($s2 as $s){
                                    ?>
                                    <dd><?=$s;?></dd>
                                    <?php
                                }
                            }
                        }
                    ?>
                </dl>             
            </div>
            
            <div id = "left">
                
                <!-- Movie's rating (top of the page, under the title) -->
                <div id ="left-top"> 
                    <?php
                    if($info[2] < 60){
                        ?>
                        <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/rottenbig.png" alt="Rotten">
                        <?php
                    }else{
                        ?>
                        <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/freshbig.png" alt="Fresh">
                        <?php
                    }
                    ?>
                    <span class="evaluation">
                        <?php print"$info[2]%"; ?>
                    </span>
                </div>
                
                <?php
                    $n = count($reviews);
                    if ($n > 10) {
                        $n = 10;
                    }
                ?>
                
                <div id="columns">
                    
                    <!-- Reviews in the left column -->
                    <div id="leftcolumn"> 
                    <?php 
                    for($i = 0; $i < ceil($n/2) ; $i++){
                        $rev = $reviews[$i];
                        ?>
                        <p class="quotes">
                            <span>
                                <?php 
                                $nomeMin = trim(strtolower($rev[1]));
                                ?>
                                <img 
                                    src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/<?=$nomeMin?>.gif" 
                                    alt="<?=$rev[1];?>"
                                >
                                <q><?=$rev[0];?></q>
                            </span>
                        </p>
                        <p class="reviewers">
                            <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/critic.gif" alt="Critic">
                            <?=$rev[2];?> 
                            <br>
                            <span class="publications"><?=$rev[3];?></span>
                        </p> 
                        <?php
                    } 
                    ?> 
                    </div>
                    
                    <!-- Reviews in the right column -->
                    <div id = "rightcolumn">
                        <?php 
                        for($i = ceil($n/2); $i < $n ; $i++){
                            $rev = $reviews[$i];
                            ?>
                            <p class="quotes">
                                <span>
                                    <?php 
                                    $nomeMin = trim(strtolower($rev[1]));
                                    ?>
                                    <img 
                                        src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/<?=$nomeMin?>.gif" 
                                        alt="<?=$rev[1];?>"
                                    >
                                    <q><?=$rev[0];?></q>
                                </span>
                            </p>
                            <p class="reviewers">
                                <img src="http://www.cs.washington.edu/education/courses/cse190m/11sp/homework/2/critic.gif" alt="Critic">
                                <?=$rev[2];?> 
                                <br>
                                <span class="publications"><?=$rev[3];?></span>
                            </p> 
                            <?php
                        } 
                        ?>
                    </div>
                </div>
                
            </div>
            
            <!-- Number of reviews (bottom of the page)-->
            <p id="bottom">(1-<?=$n;?>) of <?=$n;?> </p>
            
        </div>
        
        <div id="validators">
            <a href="http://validator.w3.org/check/referer">
                <img src="http://webster.cs.washington.edu/w3c-html.png" alt="Validate">
            </a> 			
            <br>
            <a href="http://jigsaw.w3.org/css-validator/check/referer">
                <img src="http://jigsaw.w3.org/css-validator/images/vcss" alt="Valid CSS!">
            </a>
        </div>
        
    </body>
</html>
